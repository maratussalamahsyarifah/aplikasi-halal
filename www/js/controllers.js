angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope, $location, $ionicModal) {

    $scope.kategori = function(choice) {
        localStorage.clear();
        localStorage.setItem('kategori', choice);
        $location.path('/1');
    }

    $ionicModal.fromTemplateUrl('about', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    // Cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    $scope.$on('modal.hidden', function() {
        // Do Nothing
    });
    $scope.$on('modal.removed', function() {
        // Do Nothing
    });    

})

.controller('CekNamaCtrl', function($scope, $location) {

    $scope.kategori = localStorage.getItem('kategori');
    $scope.cariNama = function(choice) {
        localStorage.setItem('cari', choice);
        $location.path('/2');
    }

})

.controller('CekHasilCtrl', function($scope, $location, $http, $ionicLoading, $ionicPopup, $window) {

    $ionicLoading.show({
        template: 'Sedang Memuat Data...',
        showBackdrop: true
    });

    var cari = localStorage.getItem('cari');
    var kategori = localStorage.getItem('kategori');

    if (kategori == 'produk' || kategori == 'produsen') {
        kategori = 'nama_' + kategori;
    } else {
        kategori = 'nomor_' + kategori;
    }

    $http({
        method: 'GET',
        unique: true,
        url: 'http://diskes.jabarprov.go.id/api/produk_halal_mui.php?menu=' + kategori + '&query=' + cari + '&page='
    })
        .then(function successCallback(response) {
                if (response.data.status == 'error') {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Info',
                        template: 'Maaf data tidak ditemukan :('
                    });
                    // $location.path('/home');
                    $window.history.back('-1');
                } else {
                    $ionicLoading.hide();
                    $scope.contents = response.data.data;
                    // console.log(response.data.data);
                }
            },
            function errorCallback(response) {
                console.log(respoonse);
                $ionicLoading.hide();
                $ionicLoading.show({
                    template: 'Sedang Memuat Data...',
                    showBackdrop: true
                });
            });
});